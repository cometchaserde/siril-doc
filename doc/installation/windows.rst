#################################
Installation on Microsoft Windows
#################################

.. toctree::
   :hidden:

Installation with the installer
===============================

The recommended way to install Siril is to use the provided installer which will guide you step by step.

.. figure:: ../_images/installation/windows/installer_start.png
   :alt: installer_start
   :class: with-shadow
    
   First screen of the installer, you need to accept the agreement to continue.
    
.. figure:: ../_images/installation/windows/installer_end.png
   :alt: installer_end
   :class: with-shadow
    
   Last screen of the installer. You can chose to start Siril right after installation, and to open the tutorial explaining the first steps.
    
The Siril setup wizard will install all the necessary files in the right place and at the end you will have the choice to create or not a shortcut on the desktop.

.. note::
   Siril will be installed in C:\\Program Files\\Siril. If you don't have the rights to install to this folder, use a portable version instead (see :ref:`portable_binary`.)
   
.. _portable_binary:

Installation of the portable binary
===================================

If you want to use Siril without installing all kinds of files on your computer (for example if you don't have administrator permissions on the machine), then it is recommended to use the portable version. It comes in the form of a zip file, which you just have to extract to the location of your choice, then go to the ``bin`` folder to run ``siril.exe``. You can also create a shortcut on your desktop to make it easier to launch the application.

.. warning::
   Be careful, under no circumstances you should move the ``exe`` file, or any other file. Otherwise Siril will not run.

Installation from the Microsoft Store
=====================================

It is now possible to install Siril via the `Microsoft Store <https://apps.microsoft.com/store/detail/siril/XPDM23ZQ9CCLVF>`_.

.. |windows-logo| image:: ../_images/installation/windows/windows-logo.png

#. Go to the Start |windows-logo| button, and then from the apps list select Microsoft Store.

#. Type Siril in the search entry.

#. Open the page corresponding to Siril, and then select Get.

.. figure:: ../_images/installation/windows/microsoft-store.png
   :alt: microsoft-store
   :class: with-shadow
   :width: 100%

.. note:: 
   However, it is important to note that Siril updates in the Store are usually done with a small delay due to the upload process which is quite complex.

Building on Windows with Msys2
==============================

These instructions are made for compiling on Windows with MSYS2 distribution using MinGW. MSYS2 requires 64 bit Windows 7 or newer, and does not work with FAT filesystems.

`Download MSYS2 64bit <https://www.msys2.org/>`_, a software distribution and building platform for Windows and run the installer ``x86_64`` for 64-bit. When asked, specify the directory where MSYS2 64-bit will be installed.

.. |mingw64-logo| image:: ../_images/installation/windows/mingw64.ico

Run MSYS2 directly from the installer or later |mingw64-logo| **MSYS2 MinGW 64-bit** from Start menu or shortcut.

.. warning::
   Make sure to launch MinGW 64-bit (check that the icon is blue at the top of the terminal window).

First, update the package database and core system packages by typing (for more info about pacman see `this page <https://wiki.archlinux.org/index.php/pacman>`_)::

    pacman -Syu

Installing dependencies
~~~~~~~~~~~~~~~~~~~~~~~

.. note::
   Automake is the legacy (stable) build method, now being replaced by meson (experimental) build system.
   
To install the dependencies, enter the following command::

    pacman --noconfirm -S --needed base-devel \
    mingw-w64-x86_64-toolchain \
    mingw-w64-x86_64-cmake \
    git \
    automake \
    mingw-w64-x86_64-lcms2 \
    mingw-w64-x86_64-curl \
    mingw-w64-x86_64-json-glib \
    mingw-w64-x86_64-meson \
    mingw-w64-x86_64-ninja \
    mingw-w64-x86_64-fftw \
    mingw-w64-x86_64-exiv2 \
    mingw-w64-x86_64-libgit2 \
    mingw-w64-x86_64-gtk3 \
    mingw-w64-x86_64-gsl \
    mingw-w64-x86_64-opencv \
    mingw-w64-x86_64-libheif \
    mingw-w64-x86_64-ffms2 \
    mingw-w64-x86_64-cfitsio \
    mingw-w64-x86_64-libraw
   
Building from source
~~~~~~~~~~~~~~~~~~~~

The source code is stored on a gitlab repository, you can download it with this command the first time::

   git clone https://gitlab.com/free-astro/siril.git
   cd siril
   git submodule update --init

Now, generate the build system and compile the code by typing::

   meson setup _build --buildtype release
   ninja -C _build install
   
To launch your build of Siril, run MSYS2 64-bit and type siril's command name::

   siril

You can also create a shortcut to siril.exe to start it, the default location is ``/mingw64/bin/``.

To update your version, run MSYS2 64-bit then::

   pacman -Syu
   cd siril
   git pull --recurse-submodules
   meson setup _build --reconfigure
   ninja -C _build && ninja -C _build install
   
If ``git pull`` does not show any change, there is no need to rebuild by running the make command. Otherwise, it will update your build.

After that just launch the build by typing::
 
   siril
