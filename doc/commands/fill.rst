| Fills the loaded image entirely or only the selection if there is one with pixels having the **value** intensity expressed in ADU
