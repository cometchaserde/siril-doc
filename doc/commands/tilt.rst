| Computes the sensor tilt as the FWHM difference between the best and worst corner truncated mean values. The **clear** option allows to clear the drawing
