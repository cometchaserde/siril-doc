| Sets the extension used and recognized by sequences.
| 
| The argument **extension** can be "fit", "fts" or "fits"
