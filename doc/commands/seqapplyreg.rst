| Applies geometric transforms on images of the sequence given in argument so that they may be superimposed on the reference image, using registration data previously computed (see REGISTER).
| 
| The output sequence name starts with the prefix **"r\_"** unless otherwise specified with **-prefix=** option.
| 
| The option **-drizzle** activates up-scaling by 2 the images created in the transformed sequence.
| 
| The pixel interpolation method can be specified with the **-interp=** argument followed by one of the methods in the list **no**\ [ne], **ne**\ [arest], **cu**\ [bic], **la**\ [nczos4], **li**\ [near], **ar**\ [ea]}. If **none** is passed, the transformation is forced to shift and a pixel-wise shift is applied to each image without any interpolation.
| Clamping of the bicubic and lanczos4 interpolation methods is the default, to avoid artefacts, but can be disabled with the **-noclamp** argument.
| 
| The registration is done on the first layer for which data exists for RGB images unless specified by **-layer=** option (0, 1 or 2 for R, G and B respectively).
| 
| Automatic framing of the output sequence can be specified using **-framing=** keyword followed by one of the methods in the list { current \| min \| max \| cog } :
| **-framing=max** (bounding box) adds a black border around each image as required so that no part of the image is cropped when registered.
| **-framing=min** (common area) crops each image to the area it has in common with all images of the sequence.
| **-framing=cog** determines the best framing position as the center of gravity (cog) of all the images.
| 
| Filtering out images:
| Images to be registered can be selected based on some filters, like those selected or with best FWHM, with some of the **-filter-\*** options.
| 
| 
| Links: :ref:`register <register>`
