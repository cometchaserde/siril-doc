|

.. warning::

    Note that the live stacking commands put Siril in a state in which it's not 
    able to process other commands. After START_LS, only LIVESTACK, STOP_LS and 
    EXIT can be called until STOP_LS is called to return Siril in its normal, 
    non-live-stacking, state.
