.. code-block:: text

    resample { factor | -width= | -height= } [-interp=] [-noclamp]