.. code-block:: text

    seqplatesolve sequencename [image_center_coords] [-noflip] [-focal=] [-pixelsize=] [-limitmag=[+-]] [-catalog=] [-localasnet] [-downscale]