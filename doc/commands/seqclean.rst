| This command clears selection, registration and/or statistics data stored for the sequence **sequencename**.
| 
| You can specify to clear only registration, statistics and/or selection with **-reg**, **-stat** and **-sel** options respectively. All are cleared if no option is passed
