| Extracts **NbPlans** planes of wavelet domain of the loaded image.
| See also WAVELET and WRECONS. For color extraction, see SPLIT
| 
| Links: :ref:`wavelet <wavelet>`, :ref:`wrecons <wrecons>`, :ref:`split <split>`
