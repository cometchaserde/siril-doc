| Computes and applies a linear function between a **reference** image and the loaded image.
| 
| The algorithm will ignore all reference pixels whose values are outside of the [**low**, **high**] range
