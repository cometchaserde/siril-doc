| Same command as MTF but for the sequence **sequencename**.
| 
| The output sequence name starts with the prefix "mtf\_" unless otherwise specified with **-prefix=** option
| 
| Links: :ref:`mtf <mtf>`
