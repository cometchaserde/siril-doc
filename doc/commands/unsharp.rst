| Applies an unsharp mask, actually a Gaussian filtered image with sigma **sigma** and a blend with the parameter **amount** used as such: out = in \* (1 + amount) + filtered \* (-amount).
| 
| See also GAUSS, the same without blending
| 
| Links: :ref:`gauss <gauss>`
