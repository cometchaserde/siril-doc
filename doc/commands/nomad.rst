| Displays stars from the local catalog by default, for the loaded plate solved image, in GUI only, down to the provided **limit_magnitude** (13 by default).
| An alternate online catalog can be specified with **-catalog=**, taking values tycho2, nomad, gaia, ppmxl, brightstars, apass.
| Stars with no B-V information will be kept; they can be excluded by passing **-photo**
