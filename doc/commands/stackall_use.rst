.. code-block:: text

    stackall
    stackall { sum | min | max } [filtering]
    stackall { med | median } [-nonorm, norm=] [-filter-incl[uded]]
    stackall { rej | mean } [rejection type] [sigma_low sigma_high] [-nonorm, norm=] [filtering] [ -weight_from_noise | -weight_from_wfwhm | -weight_from_nbstars | -weight_from_nbstack ] [-rgb_equal] [-out=filename]