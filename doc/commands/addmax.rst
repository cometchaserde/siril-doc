| Computes a new image combining the image in memory with the image **filename**. At each pixel location, the new value is determined as the max of value in current image and in **filename**
