Photometry
##########

This section introduces you to all the utilities related to photometry, first 
explaining the principles of photometry, then how it is used in Siril.

Siril is able to determine the magnitude of stars as well as its uncertainty. 
From there it is possible to study the variability of certain stars, exoplanets,
or occultations. A light curve is also built at the end of the process.


.. figure:: ./_images/photometry/photometry.png
   :alt: dialog
   :class: with-shadow
   :width: 100%

   Example of exoplanet photometry in Siril.

.. toctree::
   :hidden:

   photometry/general
   photometry/quickphotometry
   photometry/lightcurves
