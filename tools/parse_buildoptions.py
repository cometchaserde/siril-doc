from subprocess import Popen, PIPE, STDOUT
import os, sys
import re
from pathlib import Path

#########################
# get current branch name
#########################
def get_active_branch_name():
    head_dir = Path(".") / ".git" / "HEAD"
    with head_dir.open("r") as f: content = f.read().splitlines()
    for line in content:
        if line[0:4] == "ref:":
            branch = line.partition("refs/heads/")[2]
            if branch == 'main': return 'master'
            return branch

################
# get orgin repo
################
def get_repo_url():
    p = Popen('git config --get remote.origin.url', stdout=PIPE, stdin=PIPE, stderr=STDOUT, shell=True)
    log = p.communicate()[0].decode()
    baseurl = log.split('-doc.git')[0]
    if baseurl.startswith('git@gitlab.com:'):
        baseurl = baseurl.replace('git@gitlab.com:','https://gitlab.com/')
    return baseurl


#GLOBAL VARIABLES

repo_url = get_repo_url()
branchname = get_active_branch_name()
page_options = '{:s}/-/raw/{:s}/meson_options.txt?inline=false'.format(repo_url, branchname)

loc = os.path.normpath(os.path.join(os.path.dirname(__file__),'../doc'))
os.chdir(loc)
iswin = sys.platform.startswith('win32')


######
# Main
######
if iswin:
    getcmd = "C:\msys64\msys2_shell.cmd -mingw64 -mintty -where \"{:s}\" -c \"wget -nv '{:s}' -O - | perl -0777 -pe 's/<script.*?script>//gs' | perl -pe 's/<link .*?\/>//gs' > meson_options.txt\"".format(loc, page_options)
else:
    getcmd = "wget -nv '{:s}' -O - | perl -0777 -pe 's/<script.*?script>//gs' | perl -pe 's/<link .*?\/>//gs' > {:s}/meson_options.txt".format(page_options, loc)


p = Popen(getcmd, stdout=PIPE, stdin=PIPE, stderr=STDOUT, shell=True)
p.communicate()


with open(os.path.join(loc,'meson_options.txt'), 'r', encoding="utf8") as fin:
    rawlines = fin.readlines()

lines = [l.strip() for l in rawlines if l.strip()]
options = '\n'.join(lines)

pattern = r'option\(([^)]*)\)'
result = re.findall(pattern, options)
#block_contents = ["{{option: {:s}}}".format(match) for match in result]
opts = [match.split('\n', 1)[0].strip(" ',") for match in result]
vals = [match.split('\n', 1)[1].strip() for match in result]

# test = [eval(b) for b in block_contents]
opt={}
out = []
for o,v in zip(opts,vals):
    vv = v.split('\n')
    fields={}
    for vvv in vv:
        a,b = vvv.split(':',1)
        if b[-1] == ',':
            b = b.rsplit(",", 1)[0]
        fields[a.strip(" '")] = b.strip(" '")
    if not 'choices' in fields.keys():
        fields['choices'] = 'N/A'
    out += ['{:s}\t{:s}\t{:s}\t{:s}\t{:s}'.format(o, fields['type'], fields['value'], fields['choices'], fields['description'])]

with open(os.path.join(loc, 'installation', 'build_options.txt'),'wb') as f:
    f.write('Option\tType\tValue\tChoices\tDescription\n'.encode('utf8'))
    f.write('\n'.join(out).encode('utf8'))

os.remove(os.path.join(loc,'meson_options.txt'))